var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var address_dal = require('../model/address_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');
var resume_dal = require('../model/resume_dal');


// View All companys
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});


// View the company for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
            var account_id = result[0][0].account_id;
            account_dal.getById(account_id, function(err, result1) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('resume/resumeViewById', {result: result, account: result1});
                }
            })
        });
    }
});

router.get('/add', function(req, res) {
    if (req.query.account_id == null) {
        res.send('A name must be chosen');
    }
    else {
        var account_id = req.query.account_id;
            account_dal.getId(account_id, function(err, result){
            skill_dal.getByAccountId(account_id, function (err, result1) {
                company_dal.getByAccountId(account_id, function (err, result2) {
                    school_dal.getByAccountId(account_id, function (err, result3) {

                        console.log(result3);
                        if (err) {
                            res.send(err);
                        }
                        else {
                            res.render('resume/resumeAdd', {
                                'company': result2,
                                'school': result3,
                                'skill': result1,
                                'accountid': result});
                        }
                    })
                })
            });
        });
    }
});

// Return the add a new company form
router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAddSelectUser', {'account': result});
        }
    });
});


router.post('/insert', function(req, res){
    var account_id = req.body.account_id;

    if(req.body.account_id == null){
        res.send('Account ID is required.');
    }
    if(req.body.company_id == null) {
        res.send('Company Name must be provided.');
    }
    else if(req.body.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else if(req.body.school_id == null) {
        res.send('At least one school must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.send('success');
            }
        });
    }
});


router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, result){
        res.redirect(302, '/resume/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('An resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result) {
            resume_dal.getById(req.query.resume_id, function(err, result4) {
                skill_dal.getByResumeId(req.query.resume_id, function (err, result1) {
                    company_dal.getByResumeId(req.query.resume_id, function (err, result2) {
                        school_dal.getByResumeId(req.query.resume_id, function (err, result3) {
                            res.render('resume/resumeUpdate', {
                                skill: result1,
                                school: result3,
                                company: result2,
                                resume: result[0][0]
                            });
                        })
                    })
                })
            })

        });
    }

});

module.exports = router;
