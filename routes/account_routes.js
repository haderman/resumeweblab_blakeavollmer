var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var address_dal = require('../model/address_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');




// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res) {
    if (req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getCompany(req.query.account_id, function (err, result1) {
            account_dal.getSchool(req.query.account_id, function (err, result2) {
                account_dal.getSkill(req.query.account_id, function (err, result3) {
                    account_dal.getAccount(req.query.account_id, function (err, result4) {
                        if (err) {
                            res.send(err);
                        }
                        else {
                            res.render('account/accountViewById', {
                                'company': result1, 'school': result2,
                                'skill': result3, 'account': result4
                            });
                        }

                    });

                });
            });
        });
    }
});

// Return the add a new account form
router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    skill_dal.getAll(function (err, result1) {
        company_dal.getAll(function (err, result2) {
            school_dal.getAll(function (err, result3) {

                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('account/accountAdd', {'company': result2, 'school': result3, 'skill': result1});
                    }
                })
            })
        })


});

// View the address for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('A first name must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('A last name must be provided.');
    }
    else if(req.query.school_id == null) {
        res.send('At least one school must be provided.');
    }
    else if(req.query.email == null) {
        res.send('An e-mail must be provided.');
    }
    else if(req.query.last_name == null) {
        res.send('A last name must be provided.');
    }

    else if(req.query.company_id == null) {
        res.send('At least one company must be provided.');
    }

    else if(req.query.skill_id == null) {
        res.send('At least one skill must be provided.');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result1) {
            company_dal.getAll(function(err, result2){
                skill_dal.getAll(function (err,result3){
                    school_dal.getAll(function (err, result4){
                        res.render('account/accountUpdate', {account:result1[0][0], skill:result3, company:result2, school:result4});
                    })
                })
            })

        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        account_dal.getById(req.query.account_id, function(err, company){
            account_dal.getAll(function(err, company) {
                res.render('account/accountUpdate', {account: account});
            });
        });
    }

});

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
        res.redirect(302, '/account/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {

            account_dal.accountDeleteAll(req.query.account_id, function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    //poor practice, but we will handle it differently once we start using Ajax
                    res.redirect(302, '/account/all');
                }
            });
        }

});

module.exports = router;