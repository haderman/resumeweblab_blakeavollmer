var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT a.*, c.company_name FROM company c ' +
        'LEFT JOIN company_address ca on ca.company_id = c.company_id ' +
        'LEFT JOIN address a on a.address_id = ca.address_id ' +
        'WHERE a.address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        if (result.length == 0){
            var newquery = 'SELECT a.* FROM address a WHERE a.address_id = ?';
            connection.query(newquery, queryData, function(err, result){
                console.log(result);
                callback (err, result);
            })

        }
        else{
            console.log(result);
        callback(err, result);}
    });
};

exports.insert = function(params, callback) {


    var query = 'INSERT INTO address (street, zip_code) VALUES (?)';

    var queryData = [params.street, params.zip_code];
    connection.query(query, [queryData], function (err, result) {
        callback(err, result);
    });
};


exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?';
    var query1 = 'DELETE FROM address_company WHERE address_id = ?';
    var query2 = 'UPDATE school SET school.address_id = null WHERE address_id = ?';
    var queryData = [address_id];

    connection.query(query2, queryData, function(err, result){
        connection.query(query1, queryData, function(err, result){
            connection.query(query, queryData, function(err, result) {
                callback(err, result);
            })
    })


    });

};

/*
//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < params.address_id.length; i++) {
            companyAddressData.push([company_id, params.address_id[i]]);
        }
    }
    else {
        companyAddressData.push([company_id, params.address_id]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;*/

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ?, zip_code = ? WHERE address_id = ?';
    var queryData = [params.street, params.zip_code, params.address_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });

};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};