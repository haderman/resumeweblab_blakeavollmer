var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

var getCompany = function(account_id, callback){
    var query = 'SELECT DISTINCT company_name FROM company, account_company WHERE account_company.account_id = (?) AND company.company_id = account_company.company_id;' ;
    connection.query(query, account_id, function(err,result){
        callback(err, result);
    })
}
module.exports.getCompany = getCompany;

var getSkill = function(account_id, callback){
    var query = 'SELECT DISTINCT skill_name FROM skill, account_skill WHERE account_skill.account_id = (?) AND skill.skill_id = account_skill.skill_id;' ;
    connection.query(query, account_id, function(err,result){
        callback(err, result);
    })
}
module.exports.getSkill = getSkill;


var getSchool = function(account_id, callback){
    var query = 'SELECT DISTINCT school_name FROM school, account_school WHERE account_school.account_id = (?) AND school.school_id = account_school.school_id;' ;
    connection.query(query, account_id, function(err,result){
        callback(err, result);
    })
}
module.exports.getSchool = getSchool;


var getAccount = function(account_id, callback){
    var query = 'SELECT first_name, last_name, email FROM account WHERE account_id = (?)' ;
    connection.query(query, account_id, function(err,result){
        callback(err, result);
    })
}
module.exports.getAccount = getAccount;


exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getId = function(account_id, callback){
    var query = 'SELECT account_id FROM account a WHERE a.account_id = ?';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    })
}

exports.getById = function(account_id, callback) {
    //var query = 'CALL account_getInfo(?)';
    var query1 = 'SELECT DISTINCT company_name FROM company, account_company WHERE account_company.account_id = (?) AND company.company_id = account_company.company_id;' ;
    var query2 = 'SELECT DISTINCT skill_name FROM skill, account_skill WHERE account_skill.account_id = (?) AND skill.skill_id = account_skill.skill_id;' ;
    var query3 = 'SELECT DISTINCT school_name FROM school, account_school WHERE account_school.account_id = (?) AND school.school_id = account_school.school_id;' ;
    var query4 = 'SELECT first_name, last_name, email FROM account WHERE account_id = (?)' ;
    var queryData = [account_id];




    connection.query(query1, queryData, function(err, result) {
        if (err) throw err;
        console.log(result);
        connection.query(query2 ,queryData, function(err, result){
            if (err) throw err;
            console.log(result);
            connection.query(query3, queryData, function(err, result){
                if (err) throw err;
                console.log(result);
                connection.query(query4, queryData, function(err, result){
                    console.log(result);

                    callback(err, result);
                })

            })
        })
    });
};

exports.insert = function(params, callback) {

    // First, I insert the values into account. This works with no issues.
    var query = 'INSERT INTO account (first_name, last_name, email) VALUES (?)';

    var queryData = [params.first_name, params.last_name, params.email];

    connection.query(query, [queryData], function (err, result) {
        //Once it gets to this point, account_id is passed over correctly. No issues here.
        var account_id = result.insertId;

        // Based on my research, there isn't a way to do a single sql statement to insert
        // multiple records, so I have created 3 query variables. These initialize with no issues.
        var query1 = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
        var query2 = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
        var query3 = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

        // I use this for creating an array of the school_id's that are passed over.
        // The if statement fills the array with multiple values if multiple are selected
        // and the else statement puts a single value into a length 1 array if only one is selected
        // I repeat this process for accountSchoolData, accountSkillData, and accountCompanyData.
        // There are no issues with populating the arrays.
        var accountSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                accountSchoolData.push([account_id, params.school_id[i]]);
            }
        }
        else {
            accountSchoolData.push([account_id, params.school_id[0]]);
        }
        var accountSkillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                accountSkillData.push([account_id, params.skill_id[i]]);
            }
        }
        else {
            accountSkillData.push([account_id, params.skill_id[0]]);
        }
        var accountCompanyData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                accountCompanyData.push([account_id, params.company_id[i]]);
            }
        }
        else {
            accountCompanyData.push([account_id, params.company_id[0]]);
        }
        // This seems to be where the issue comes up. I have 3 seperate queries that are supposed
        // to insert the records based on which query var and array is passed. I based much of this
        // code (here and above) on the company_dal's insert function.
        // None of the values that are in the array are being passed into their respective tables.
        // Can you tell me a better way to do make this work?
        connection.query(query1, [accountSchoolData], function(err, result){
            if(err) throw err;
            console.log(result);


            connection.query(query2, [accountSkillData], function(err, result){
                if(err) throw err;
                console.log(result);

                connection.query(query3, [accountCompanyData], function(err, result){
                    console.log(result);
                    callback(err, result);
                });
            });
        });
    });
};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, result) {
        accountDeleteAll(account_id, function(err, result){
            callback(err, result);}
            );
    });

};

var accountSchoolInsert = function(account_id, schoolIdArray, callback){
    var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
    connection.query(query, account_id, schoolIdArray, function(err,result){
       callback(err, result);
    })

};
//export the same function so it can be used by external callers
module.exports.accountSchoolInsert = accountSchoolInsert;

var accountSkillInsert = function(account_id, skillIdArray, callback){
    var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
    connection.query(query, account_id, skillIdArray, function(err,result){
        callback(err, result);
    })

};
module.exports.accountSkillInsert = accountSkillInsert;

var accountCompanyInsert = function(account_id, companyIdArray, callback){
    var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

    connection.query(query, account_id, companyIdArray, function(err,result){
        callback(err, result);
    })
};
module.exports.accountCompanyInsert = accountCompanyInsert;

var accountDelete = function(account_id, callback) {
    var query = 'DELETE a.* FROM account a WHERE a.account_id = (?)';
    var queryData = [account_id];
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};
module.exports.accountDelete = accountDelete;

var accountDeleteSSC = function(account_id, callback){
    var query = 'DELETE a.*, c.*, b.*, r.* FROM account_skill a  ' +
        'LEFT JOIN account_school b ON b.account_id = ? ' +
        'LEFT JOIN account_company c ON c.account_id = ?' +
        'LEFT JOIN resume r ON r.account_id = ?' +
        'LEFT JOIN account d ON d.account_id = ? WHERE a.account_id = ?';
    var queryData = [account_id, account_id, account_id, account_id, account_id]

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    })
}
module.exports.accountDeleteSSC = accountDeleteSSC;

//declare the function so it can be used locally
var accountDeleteAll = function(account_id, callback){
    var query = 'DELETE a.*, c.*, b.*, r.*, d.* FROM account_skill a  ' +
    'LEFT JOIN account_school b ON b.account_id = ? ' +
    'LEFT JOIN account_company c ON c.account_id = ?' +
    'LEFT JOIN resume r ON r.account_id = ?' +
    'LEFT JOIN account d ON d.account_id = ? WHERE a.account_id = ?';
    var queryData = [account_id, account_id, account_id, account_id, account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.accountDeleteAll = accountDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE account SET first_name = ?, last_name = ?, email = ? WHERE account_id = ?';
    var queryData = [params.first_name, params.last_name, params.email, params.account_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        accountDeleteSSC(params.account_id, function(err, result){
            var account_id = params.account_id;
            var query1 = 'INSERT INTO account_school (account_id, school_id) VALUES ?';
            var query2 = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
            var query3 = 'INSERT INTO account_company (account_id, company_id) VALUES ?';

            var accountSchoolData = [];
            if (params.school_id.constructor === Array) {
                for (var i = 0; i < params.school_id.length; i++) {
                    accountSchoolData.push([account_id, params.school_id[i]]);
                }
            }
            else {
                accountSchoolData.push([account_id, params.school_id[0]]);
            }
            var accountSkillData = [];
            if (params.skill_id.constructor === Array) {
                for (var i = 0; i < params.skill_id.length; i++) {
                    accountSkillData.push([account_id, params.skill_id[i]]);
                }
            }
            else {
                accountSkillData.push([account_id, params.skill_id[0]]);
            }
            var accountCompanyData = [];
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    accountCompanyData.push([account_id, params.company_id[i]]);
                }
            }
            else {
                accountCompanyData.push([account_id, params.company_id[0]]);
            }

            connection.query(query1, [accountSchoolData], function(err, result){
                if(err) throw err;
                console.log(result);
                connection.query(query2, [accountSkillData], function(err, result){
                    if(err) throw err;
                    console.log(result);
                    connection.query(query3, [accountCompanyData], function(err, result){
                        console.log(result);
                        callback(err, result);
                    });
                });
            });

        })

    });
};


exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};





