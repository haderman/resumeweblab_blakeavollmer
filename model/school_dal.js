var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM school;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getByAccountId = function(account_id, callback){
    var query = 'SELECT sa.*, s.school_name FROM account_school sa ' +
        'LEFT JOIN school s ON sa.school_id = s.school_id ' +
        'WHERE sa.account_id = ?';
    var queryData = account_id;
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });

};

exports.getByResumeId = function(resume_id, callback){
    var query = 'SELECT rs.*, s.school_name FROM resume_school rs ' +
        'LEFT JOIN school s ON rs.school_id = s.school_id ' +
        'WHERE rs.resume_id = ?';
    var queryData = resume_id;
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });

};

exports.getById = function(school_id, callback) {
    var query = 'SELECT s.*, a.street, a.zip_code FROM school s ' +
        'LEFT JOIN address a on a.address_id = s.address_id ' +
        'WHERE s.school_id = ?';
    var queryData = [school_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

/*exports.insert = function(params, callback) {
    var query = 'INSERT INTO school (school_name, address_id) VALUES ?';

    var queryData = [params.school_name, params.address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};*/
exports.insert = function(params, callback) {


    var query = 'INSERT INTO school (school_name, address_id) VALUES (?)';

    var queryData = [params.school_name, params.address_id];

    connection.query(query, [queryData], function(err, result){
        callback(err, result);
    });


};


exports.delete = function(school_id, callback) {
    var query1 = 'DELETE FROM account_school WHERE school_id = ?';
    var query = 'DELETE FROM school WHERE school_id = ?';
    var queryData = [school_id];
connection.query(query1, queryData, function(err, result){
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
})


};


//declare the function so it can be used locally
var schoolAddressDeleteAll = function(school_id, callback){
    var query = 'DELETE * FROM school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.schoolAddressDeleteAll = schoolAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE school SET school_name = ? WHERE school_id = ?';
    var queryData = [params.school_name, params.school_id];
    var address_id = params.address_id;
    var queryData2 = [params.school_name, params.address_id, params.address_id];
    var school_name = params.school_name;
    var school_id = params.school_id;
    connection.query(query, queryData, function(err, result){
        if (params.address_id.length > 1){
            var query1 = 'UPDATE school SET address_id = ? WHERE school_id = ?';
            connection.query(query1, [queryData2], function(err, result){
                console.log(result);
                var query2 = 'INSERT INTO school (school_name, address_id) VALUES (?) WHERE NOT EXISTS (address_id)';
                connection.query(query2, [queryData3], function(err, result){
                    callback(err, result);
                })
            })
        }
        else{
            var query1 = 'UPDATE school SET address_id = ? WHERE school_id = ?';
            var queryData1 = [address_id, school_id];
            connection.query(query1, queryData1, function(err, result) {
                callback(err, result);
            })
        }

    })


};

   /* connection.query(query, queryData, function(err, result) {

        schoolAddressDeleteAll(params.school_id, function(err, result){

            if(params.address_id != null) {

                schoolAddressInsert(params.school_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};*/



exports.edit = function(school_id, callback) {
    var query = 'CALL school_getinfo(?)';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);

    });
};