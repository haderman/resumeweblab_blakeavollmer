var mysql   = require('mysql');
var db  = require('./db_connection.js');

var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT a.*, r.* FROM resume r ' +
    'LEFT JOIN account a ON a.account_id = r.account_id ' +
    'ORDER BY a.first_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?)';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, [queryData], function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query1 = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
        var query2 = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';
        var query3 = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';


        var resumeSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                resumeSchoolData.push([resume_id, params.school_id[i]]);
            }
        }
        else {
            resumeSchoolData.push([resume_id, params.school_id[0]]);
        }
        var resumeCompanyData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                resumeCompanyData.push([resume_id, params.company_id[i]]);
            }
        }
        else {
            resumeCompanyData.push([resume_id, params.company_id[0]]);
        }
        var resumeSkillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                resumeSkillData.push([resume_id, params.skill_id[i]]);
            }
        }
        else {
            resumeSkillData.push([resume_id, params.skill_id[0]]);
        }
        // This seems to be where the issue comes up. I have 3 seperate queries that are supposed
        // to insert the records based on which query var and array is passed. I based much of this
        // code (here and above) on the company_dal's insert function.
        // None of the values that are in the array are being passed into their respective tables.
        // Can you tell me a better way to do make this work?
        connection.query(query1, [resumeSchoolData], function(err, result){
            if(err) throw err;
            console.log(result);


            connection.query(query2, [resumeCompanyData], function(err, result){
                if(err) throw err;
                console.log(result);

                connection.query(query3, [resumeSkillData], function(err, result){
                    console.log(result);
                    callback(err, result);
                });
            });
        });
    });

};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];
    var query1 = 'DELETE FROM resume_company WHERE resume_id = ?';
    var query2 = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var query3 = 'DELETE FROM resume_school WHERE resume_id = ?' ;

    connection.query(query2, queryData, function(err, result) {
        connection.query(query1, queryData, function(err, result){
            connection.query(query3, queryData, function(err, result){
                connection.query(query, queryData, function(err, result){
                    callback(err, result);

                })
            })


        })
    });

};

//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < address_id.length; i++) {
            companyAddressData.push([company_id, address_id[i]]);
        }
    }
    else {
        companyAddressData.push([company_id, addressIdArray]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        resumeDeleteSSC(params.resume_id, function(err, result){
            var resume_id = params.resume_id;
            var query1 = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';
            var query2 = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';
            var query3 = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

            var resumeSchoolData = [];
            if (params.school_id.constructor === Array) {
                for (var i = 0; i < params.school_id.length; i++) {
                    resumeSchoolData.push([resume_id, params.school_id[i]]);
                }
            }
            else {
                resumeSchoolData.push([resume_id, params.school_id[0]]);
            }
            var resumeCompanyData = [];
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    resumeCompanyData.push([resume_id, params.company_id[i]]);
                }
            }
            else {
                resumeCompanyData.push([resume_id, params.company_id[0]]);
            }
            var resumeSkillData = [];
            if (params.skill_id.constructor === Array) {
                for (var i = 0; i < params.skill_id.length; i++) {
                    resumeSkillData.push([resume_id, params.skill_id[i]]);
                }
            }
            else {
                resumeSkillData.push([resume_id, params.skill_id[0]]);
            }

            connection.query(query1, [resumeSchoolData], function(err, result){
                if(err) throw err;
                console.log(result);
                connection.query(query2, [resumeCompanyData], function(err, result){
                    if(err) throw err;
                    console.log(result);
                    connection.query(query3, [resumeSkillData], function(err, result){
                        console.log(result);
                        callback(err, result);
                    });
                });
            });
        })


    });
};

var resumeDeleteSSC = function(resume_id, callback){
    var query = 'DELETE a.*, b.*, c.* FROM resume_school a ' +
        'LEFT JOIN resume_skill b ON b.resume_id = ? ' +
        'LEFT JOIN resume_company c ON c.resume_id = ? ' +
        'WHERE a.resume_id = ?';

    var queryData = [resume_id, resume_id, resume_id]

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    })
}
module.exports.resumeDeleteSSC = resumeDeleteSSC;

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};