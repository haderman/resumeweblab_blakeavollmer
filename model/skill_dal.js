var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);


exports.getAll = function(callback) {
    var query = 'SELECT * FROM skill;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getByAccountId = function(account_id, callback){
    var query = 'SELECT sa.*, s.skill_name FROM account_skill sa ' +
        'LEFT JOIN skill s ON sa.skill_id = s.skill_id ' +
        'WHERE sa.account_id = ?';
    var queryData = account_id;
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });

};

exports.getByResumeId = function(resume_id, callback){
    var query = 'SELECT rs.*, s.skill_name FROM resume_skill rs ' +
        'LEFT JOIN skill s ON rs.skill_id = s.skill_id ' +
        'WHERE rs.resume_id = ?';
    var queryData = resume_id;
    connection.query(query, queryData, function(err, result){
        callback(err, result);
    })
}

exports.getById = function(skill_id, callback) {
    var query = 'SELECT * FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];
    //console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {


    var query = 'INSERT INTO skill (skill_name, description) VALUES (?)';

    var queryData = [params.skill_name, params.description];

    connection.query(query, [queryData], function(err, result){
        callback(err, result);
    });


};


exports.delete = function(skill_id, callback) {
    var query = 'DELETE FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


//declare the function so it can be used locally
var skillDeleteAll = function(skill_id, callback){
    var query = 'DELETE FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.skillDeleteAll = skillDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE skill SET skill_name = ?, description = ? WHERE skill_id = ?';
    var queryData = [params.skill_name, params.description, params.skill_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });

};


exports.edit = function(skill_id, callback) {
    var query = 'CALL skill_getinfo(?)';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};